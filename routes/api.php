<?php

use App\Models\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [App\Http\Controllers\AuthController::class, 'login']);
Route::post('callback', [App\Http\Controllers\SubscriptionController::class, 'callback']);
Route::get('logs', function () {
    return response()->json(Log::all());
});
Route::middleware('auth:api')->group(function () {
    Route::post('subscription', [App\Http\Controllers\SubscriptionController::class, 'subscription']);
});
