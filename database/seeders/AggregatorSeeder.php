<?php

namespace Database\Seeders;

use App\Models\Aggregator;
use Illuminate\Database\Seeder;

class AggregatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Aggregator::create([
            'name' => 'Aggregator1Name',
            'logo' => config('app.url').'/storage/icons/profile.png',
            'endpoint' => 'https://api.aggregator1.com/api/pay'
        ]);

        Aggregator::create([
            'name' => 'Aggregator2Name',
            'logo' => config('app.url').'/storage/icons/profile.png',
            'endpoint' => 'https://api.aggregator2.com/api/pay'
        ]);
    }
}
