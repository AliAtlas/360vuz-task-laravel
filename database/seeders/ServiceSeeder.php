<?php

namespace Database\Seeders;

use App\Models\Aggregator;
use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = Service::create([
            'name' => 'Service1 name',
            'description' => 'Service1 description',
            'icon' => config('app.url').'/storage/icons/magic.png',
            'price' => 500
        ]);

        $service->aggregators()->attach(Aggregator::find(1));

        $service = Service::create([
            'name' => 'Service2 name',
            'description' => 'Service2 description',
            'icon' => config('app.url').'/storage/icons/magic.png',
            'price' => 1200
        ]);

        $service->aggregators()->attach(Aggregator::find(2));
    }
}
