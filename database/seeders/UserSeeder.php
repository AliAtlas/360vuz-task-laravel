<?php

namespace Database\Seeders;

use App\Models\Aggregator;
use App\Models\User;
use Spatie\Permission\Models\Role;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Customer',
            'email' => 'customer@360vuz.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'
        ]);
    }
}
