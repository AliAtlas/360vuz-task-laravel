<?php

namespace Database\Seeders;

use App\Models\Aggregator;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            AggregatorSeeder::class,
            ServiceSeeder::class,
        ]);
    }
}
