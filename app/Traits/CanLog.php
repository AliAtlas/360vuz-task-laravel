<?php

namespace App\Traits;

use App\Models\Log;

trait CanLog
{
    public function logs()
    {
        return $this->morphMany(Log::class, Log::$relation);
    }

    public function log($details = '')
    {
        Log::create([
            'loggable_id' => $this->id,
            'loggable_type' => get_class($this),
            'details' => $details,
            'user_id' => auth()->user() ? auth()->user()->id : null
        ]);
    }
}
