<?php

namespace App\Observers;

use App\Models\Subscription;

class SubscriptionObserver
{
    /**
     * Handle the Subscription "created" event.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return void
     */
    public function created(Subscription $subscription)
    {
        $subscription->log('Subscription created');
    }

    /**
     * Handle the Subscription "updated" event.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return void
     */
    public function updated(Subscription $subscription)
    {
        if($subscription->isDirty('status')) {
            $subscription->log(
                'Subscription status updated from ' .
                $subscription->getOriginal('status') .
                ' into ' .
                $subscription->status
            );
        }
    }
}
