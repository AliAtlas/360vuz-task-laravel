<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use App\Models\Aggregator;
use App\Models\Subscription;
use App\Http\Resources\SubscriptionResource;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function subscription(Request $request)
    {
        // validating input
        $request->validate([
            'action' => ['required', 'in:sub,unsub'],
            'service_id' => ['required', 'exists:services,id'],
            'aggregator_id' => ['required', 'exists:aggregators,id'],
        ]);

        // determining required action
        if($request->action == 'sub') {
            return $this->subscribe($request);
        } else {
            return $this->unsubscribe($request);
        }
    }
    /**
     * Create a new pending subscription and calls aggregator payment processing API
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function subscribe(Request $request)
    {
        $aggregator = Aggregator::find($request->aggregator_id);

        // checking if aggregator handles chosen service
        $aggregator_handles_service = $aggregator->services()
                                        ->where('services.id', $request->service_id)
                                        ->count();

        if(! $aggregator_handles_service) {
            return response()->json([
                'error' => 'Aggregator does not handle chosen service'
            ]);
        }

        // creating subscription
        $subscription = Subscription::create([
            'user_id' => auth()->user()->id,
            'service_id' => $request->service_id,
            'aggregator_id' => $request->aggregator_id
        ]);

        // calling aggregator subscribe API
        try {
            $response = Http::withToken(config($aggregator->name.'.api-token'))->post('http://example.com/subscribe', [
                'action' => $request->action,
                'email' => auth()->user()->email, // can be replaced by another user reference
                'service' => $request->service_id, // can be replaced by another service reference
                'subscription_id' => $subscription->id,
                'callback_url' => config('app.url') . '/api/subscription/callback',
            ]);
        } catch (\Exception $exp) {

        }

        // must test if the request was successful
        return response()->json(new SubscriptionResource($subscription));
    }

    /**
     * Updates a subscription to become pending and calls aggregator payment processing API
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function unsubscribe(Request $request)
    {
        // validating input
        $request->validate([
            'action' => ['required', 'in:sub,unsub'],
            'service_id' => ['required', 'exists:services,id'],
            'aggregator_id' => ['required', 'exists:aggregators,id'],
        ]);

        $aggregator = Aggregator::find($request->aggregator_id);

        // checking if user has subscribed to the chosen service
        if(! $subscription = auth()->user()->validSubscription($request->service_id)) {
            return response()->json([
                'error' => 'You do not have a valid subscription of the chosen service'
            ]);
        }

        // updating subscription
        $subscription->update([
            'status' => 'PENDING'
        ]);

        // calling aggregator unsubscribe API
        try {
            $response = Http::withToken(config($aggregator->name.'.api-token'))->post($aggregator->endpoint, [
                'action' => $request->action,
                'email' => auth()->user()->email, // can be replaced by another user reference
                'service' => $request->service_id, // can be replaced by another service reference
                'subscription_id' => $subscription->id,
                'callback_url' => config('app.url') . '/api/subscription/callback',
            ]);
        } catch (\Exception $exp) {

        }


        // must test if the request was successful
        return response()->json(new SubscriptionResource($subscription));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function callback(Request $request)
    {
        // validating input
        $request->validate([
            'action' => ['required', 'in:sub,unsub'],
            'subscription_id' => ['required', 'exists:subscriptions,id'],
            'status' => ['required', 'in:success,fail'],
            'message' => ['required', 'string']
        ]);

        $subscription = Subscription::find($request->subscription_id);

        // checking aggregator processing status
        $subscription->updateStatus($request->action, $request->status);

        return response()->json(new SubscriptionResource($subscription));
    }
}
