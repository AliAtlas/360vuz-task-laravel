<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        $credentials = $request->only(['email', 'password']);

        $token = auth()->attempt($credentials);

        return $token ? $this->respondWithToken($token) : response()->json(['msg' => 'Unauthorized'], 401);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'iss' => config('app.url'),
            'exp' => auth()->factory()->getTTL(),
            'nbf' => now()->getTimeStamp(),
            'iat' => now()->getTimeStamp(),
            'jti' => Str::random(),
            'sub' => auth()->user()->getJWTIdentifier(),
            'aud' => config('app.url'),
        ]);
    }
}
