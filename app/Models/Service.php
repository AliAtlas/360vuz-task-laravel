<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'icon', 'price'
    ];

    protected $appends = [
        'icon_url'
    ];

    public function getIconUrlAttribute()
    {
        return config('app.url').'/'.str_replace('public', 'storage', $this->icon);
    }

    public function aggregators()
    {
        return $this->belongsToMany(Aggregator::class);
    }
}
