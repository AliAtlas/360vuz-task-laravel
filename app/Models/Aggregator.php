<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aggregator extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'logo', 'user_id', 'endpoint'
    ];

    protected $appends = [
        'logo_url'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getLogoUrlAttribute()
    {
        return config('app.url').'/'.str_replace('public', 'storage', $this->logo);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }
}
