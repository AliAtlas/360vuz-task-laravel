<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\CanLog;

class Subscription extends Model
{
    use HasFactory, CanLog;

    protected $fillable = [
        'service_id', 'user_id', 'aggregator_id', 'status'
    ];

    protected $hidden = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function aggregator()
    {
        return $this->belongsTo(Aggregator::class);
    }

    public function updateStatus($action, $status)
    {
        if($action == 'sub' && $status == 'success') {
            $this->update([
                'status' => 'SUBSCRIBED'
            ]);
        } elseif ($action == 'sub' && $status == 'fail') {
            $this->update([
                'status' => 'UNSUBSCRIBED'
            ]);
        } elseif($action == 'unsub' && $status == 'fail') {
            $this->update([
                'status' => 'SUBSCRIBED'
            ]);
        } elseif($action == 'unsub' && $status == 'success') {
            $this->update([
                'status' => 'UNSUBSCRIBED'
            ]);
        }
    }
}
