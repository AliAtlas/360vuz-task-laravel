<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    public static $relation = 'loggable';

    protected $fillable = [
        'loggable_id', 'loggable_type', 'details', 'user_id'
    ];
}
