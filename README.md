PHP 7.3

1 - git clone git@bitbucket.org:AliAtlas/360vuz-task-laravel.git

2 - php artisan key:generate

3 - import .sql file

4 - php artisan serve

5 - import .json api package to postman

6 - send (login) request and copy "access_token" response body field value

7 - pass the token as a header or as a query string parameter (Authorization : Bearer jwt) or (/endpoint?token=jwt)

8 - use subscription API to subscribe/unsubscribe to create a subscription and copy the value of "id"

9 - use callback API to accept {"status" : "success"} or deny {"status" : "fail"} an action taken on a subscription with an id of "id", and provide an explaination message

10 - use logs API to check the log
